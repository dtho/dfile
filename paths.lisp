(in-package :dfile)
;;;
;;; utilities for paths
;;;
(defun absolute-dir-p (path)
  "Return a true value if directory component of PATH is absolute."
  (let ((d (pathname-directory path)))
    (when d
      (eq (first d) :absolute))))

(defun add-dir-to-paths (dir &rest strings)
  "Return a list of path objects corresponding to STRINGS. If a member of STRINGS lacks a directory specification, use DIR for the directory in the corresponding object in the returned list. This does not implement any sort of mkdir functionality."
  (if strings
      (mapcar #'(lambda (string)
		  (let* ((path (pathname string))
			 (pathdir (pathname-directory path)))
		    (if pathdir
			path
			(merge-pathnames path dir))))
	      strings)))

(defun change-path-to-type (path type)
  "Change the path PATH so that it is of type TYPE."
  (let ((dir (pathname-directory path))
	(name (pathname-name path)))
    (make-pathname :directory dir :name name :type type)))

;; see also CL-FAD/fad.lisp DIRECTORY-PATHNAME-P and DIRECTORY-EXISTS-P
(defun directoryp (x)
  "Return a true value if X corresponds to a specification for a directory. Note: this does not test whether the directory actually exists. If X is a string, note that only '/root/foo/' is considered to point to a directory while '/root/foo' is considered to point to a file."
  (let ((pathname (if (pathnamep x) 
		      x 
		      (pathname x))))
      (let ((name (pathname-name pathname)))
	(and (not name)
	     (pathname-directory x)))))

(defun dirname (dir-path)
  "Return string corresponding to the directory name specified by path DIR-PATH."
  (car (last (pathname-directory dir-path))))

(defun dirpath-from-dirstring (string)
  "Given string STRING (implemented thus far: linux-style file paths), return the corresponding path object representing the corresponding directory."
  (pathname-directory
   (parse-namestring
    (concatenate 'string
		 (string-right-trim "/" string)
		 "/"))))

(defun dirpath->path (dirpath)
  (make-pathname :directory dirpath))

(defun filep (x)
  "Indicate whether X seems to specify a file. Note that FILEP does
not test whether file actually exists. This function should only be
used with an understanding of its limitations."
  (let ((pathname (if (pathnamep x)
		      x
		      (parse-namestring x))))
    (and (pathname-name pathname)
	 (not (equal (pathname-directory pathname)
		     '(:absolute))))))

(defun parentpath-of-dirpath (dir-path)
  "Return pathname corresponding to parent directory of DIR-PATH. If DIR-PATH specifies a file, the command will act upon the pathname corresponding to the directory containing the file. The function will return an empty path if the parent directory of the '/' (unix) directory is asked for."
  (let ((dlist (pathname-directory dir-path)))
	(make-pathname
	 :directory
	 ;; directory
	 (subseq dlist 0 (1- (list-length dlist))))))

(defun path-objects? (x)
  "Return a true value if X is a list of path objects. If not, return NIL."
  (if (consp x)
      (not (member nil (mapcar #'(lambda (path?)
			      (pathnamep path?))
			       x)))))

(defun pathnamesp (x)
  (loop for p? in x
     if (not (pathnamep p?))
     do (return nil)
     finally (return t)))

(deftype pathnames ()
  '(satisfies pathnamesp))

(defun pathspecp (x)
  (or (pathnamep x)
      (stringp x)
      (typep x 'file-stream)))

(defun string-path-to-path (path-string &optional errorp)
  "Attempt to safely read a path object from a string. Return a path
object unless PATH-STRING is not a string representation of a path
object. If PATH-STRING is not a string representation of a path
object, return NIL unless ERRORP is true."
  (declare (string path-string))
  (let ((error-message "Not a string representation of a path object."))
    (cond ((and (> (length path-string) 3)
		(string= (subseq path-string 0 2)
			 "#P"))
	   (let ((maybe-pathname
		   (with-standard-io-syntax
		     (let ((*read-eval* nil))
		       (read-from-string path-string)))))
	     (if (pathnamep maybe-pathname)
		 maybe-pathname
		 (if errorp
		     (error error-message)
		     nil))))
	  (t (if errorp
		 (error error-message)
		 nil)))))
