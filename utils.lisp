(in-package :dfile)

;; DCU/shared-math.lisp
(defun coerce-to-number (x)
  "Try and coerce X to a number based on the assumption that X is a representation of a base 10 number. Return NIL if it's too challenging...(so far only need strings so haven't implemented much else...** ). Note(s): consider PARSE-INTEGER for known integer values."
  (let
      ((coercion-effort 
	(cond
	  ;; if X contains a ( character, we may have problems...
	  ((if (stringp x)
	       (or
		(find #\( x)
		(find #\) x)))
	   nil)
	  ((stringp x)
	   (if (not (empty-string-p x))
	       (read-from-string x)))
	  ((keywordp x)
	   (read-from-string (string x)))
	  (t
	   x))))
    (if (numberp coercion-effort)
	coercion-effort)))

;; DCU/shared-stream
(defun slurp-to (stream test)
  (declare (stream stream))
  (let ((stream-element-type (stream-element-type stream)))
    (cond ((subtypep stream-element-type 'character)
           (coerce (loop for c = (read-char stream nil :eofdfile)
		      while (and (not (eq c :eofdfile))
				 (not (funcall test c)))
		      collect c) 'string)) 
          ((subtypep stream-element-type 'unsigned-byte) 
	   (let ((byte-list (loop for b = (read-byte stream nil :eofdfile)
			       while (and (not (eq b :eofdfile))
					  (not (funcall test b)))
			       collect b)))
	     (make-array (length byte-list) :element-type stream-element-type :initial-contents byte-list)))

          (t (error "Unexpected stream element type ~S." stream-element-type)))))
