(defsystem dfile
  :serial t
  :components ((:file "packages")
	       (:file "paths")
	       (:file "file")
	       (:file "temporary"))
  :depends-on (:flexi-streams))
