(in-package :cl-user)

(defpackage :dfile
  (:use :cl)
  (:documentation "The program is released under the terms of the Lisp Lesser GNU Public License http://opensource.franz.com/preamble.html, also known as the LLGPL. Copyright: David A. Thompson, 2009-2022")
  (:export
   add-dir-to-paths
   append-to-file
   assert-file-exists

   change-path-to-type

   delete+
   delete-file-if-exists
   delete-files
   dir-path
   dir-path-list
   directory-exists-p
   directory-path
   dirname
   dirnames-in-dir
   dirpaths-in-dir

   file-exists-p
   file-or-directory-exists
   file-path-list
   file-size
   file-to-charvector
   file-to-octets
   file-to-stream
   file-to-string
   filelist
   filenames-in-dirpath
   filep
   filepaths-in-dirpath
   file-to-vector

   insert-file-as-string

   lines-in-file
   load-if-present

   make-unique-path
   mktemp

   octets-to-file

   pathnamesp
   paths-in-dir

   read-all-from-file

   string-path-to-path
   string-to-file

   tail
   tempdir
   tempfile
   touch

   wd))
