(in-package :dfile)
;;;
;;; directory and file utils
;;;

;; (alexandria:WRITE-STRING-INTO-FILE) offers this functionality but requires specifying :if-exists :append

(defun append-to-file (pathname x &optional (createp t))
  "Append X to the file specified by the path object PATHNAME. If X is a string, WRITE-STRING is used. If CREATEP is true, create the file if it doesn't exist."
  (let ((file-stream (open pathname	
			   :direction :output
			   :if-exists :append
			   :if-does-not-exist (if createp :create :error))))
    (write-to-file-stream x file-stream)
    (close file-stream)))

(defun change-type (path type)
  "Change type of path PATH to type TYPE. Return the corresponding path."
  (merge-pathnames (make-pathname :type type) path))

(defgeneric write-to-file-stream (x file-stream))

(defmethod write-to-file-stream ((x string) file-stream)
  (write-string x file-stream))

;; PATHNAME-DESIGNATOR is a stream, pathname, or string
(defun assert-file-exists (pathname-designator)
  "Return the truename for the file."
  (let ((truename? (truename pathname-designator)))
    (assert (or (stringp truename?)
		(pathnamep truename?)))))

;; COPY-FILE: use UIOP:COPY-FILE

(defun copy-files-to-dirs (file-path-list dir-path-list)
  "Copy files specified by FILE-PATH-LIST to directories specified by DIR-PATH-LIST. This function assumes items in DIR-PATH-LIST are directories. This function does not necessarily return an error if it fails (e.g., if the directories in DIR-PATH-LIST only exist hypothetically...)."
  (dolist (target-dir dir-path-list) 
    (dolist (source file-path-list)
      (uiop:copy-file 
       source
       (merge-pathnames 
	(make-pathname
	 :name (pathname-name source)
	 :type (pathname-type source))
	(car (directory target-dir)))))))

(defgeneric delete+ (x errorp recursep)
  (:documentation "RM-like functionality. Provides a generic DELETE facility for deleting files and/or directories. X is either a pathname or a namestring corresponding to a file or directory. ERRORP indicates whether an error should be signalled if the request cannot be completed. RECURSEP is ignored if X specifies a file; if X specifies a directory, RECURSEP indicates whether to attempt recursive delete if X is a directory and dir isn't empty."))

(defmethod delete+ (x (errorp null) recursep)
  (ignore-errors (delete+ (x errorp recursep))))

(defmethod delete+ (x (errorp (eql t)) (recursep null))
  (cond ((filep x)
	 (delete-file x))
	((directoryp x)
	 (delete-directory x :recursivep recursep))
	(t
	 (error "Not sure what to do"))))

(defun delete-directory (dir-spec &key recursivep)
  (sb-ext:delete-directory dir-spec :recursive recursivep))

(defun delete-file-if-exists (file)
  "Attempt to delete file FILE. If it doesn't exist, move on. Return value undefined."
  (when (file-exists-p file)
    (delete-file file)))

(defun delete-files (files)
  (map nil 
       (lambda (file) (delete-file file))
       files))

(defun directory-exists-p (pathname)
  ;; close? will be 0 if successful
  (let ((close?
	 (ignore-errors
	   (sb-posix:opendir pathname))))
    (if close?
	(sb-posix:closedir close?)))) 

(defun dirnames-in-dir (dir-path)
  "Given directory specified by the path DIR-PATH, return a list which contains the names of the directories in the directory."
  (let ((paths (dirpaths-in-dir dir-path)))
    (mapcar #'(lambda (path)
		(car (last path))) 
	    paths)))

(defun dirpaths-in-dir (dir-path)
  "Given directory specified by the path DIR-PATH, return a list of the pathnames of the directories in the directory. Return nil if directory does not contain any subdirectories."
  (uiop:subdirectories dir-path))

;; ENSURE-DIRECTORIES-EXIST: use builtin Common Lisp (ensure-directories-exist ...)

(defun file-or-directory-exists (pathname)
  "Given pathname or string PATHNAME, return a true value if corresponding file or directory exists. Otherwise return NIL."
  (or (file-exists-p pathname)
      (directory-exists-p pathname)))

(defun file-exists-p (pathspec)
  "Only return a true value if PATHSPEC corresponds to an existing file. Do not return true if PATHSPEC specifies a directory."
  (and (ignore-errors (pathname pathspec))
       (not (directoryp pathspec))
       (uiop:file-exists-p pathspec)))

;; http://rosettacode.org/wiki/File_size#Common_Lisp 2014-09-06 (content available under GNU Free Documentation License 1.2)
(defun file-size (path)
  "Return an integer. Return NIL if the file does not exist."
  (with-open-file (stream path
			  :direction :input
			  :if-does-not-exist nil)
    ;; FIXME: what is up w/the PRINT?
    (print (if stream (file-length stream) 0))))

(defun file-to-charvector (pathname)
  "Given pathname PATHNAME specifying a file, return a character vector corresponding to the content of the file (assumes file is a readable file containing only string data). Return nil if file non-existent. Return error if file contains unreadable data."
  (if (probe-file pathname)
      ;; file exists - get string
      (progn
	(let (;; stream to use
	      (st (open pathname	
			:direction :input
			:if-does-not-exist :error))
	      (v (make-array 1 		; how to choose init size?
			     :adjustable t
			     :fill-pointer 0)))
	  ;; read characters in
	  (do ((x (read-char st nil "eof") (read-char st nil "eof")))
	      ;; end 
	      ((equal x "eof")
	       ;; end body
	       )
	    ;; push character onto return vector
	    (vector-push-extend x v) 
	    )
	  (close st)
	  ;; return v
	  v))))

;; support files which contain binary/octet data (e.g., PDF files)
(defun file-to-stream2 (pathname stream)
  "Supports files which contain binary/octet data (e.g., PDF files). Expect STREAM to be able to handle octet data."
  (octets-to-stream (file-to-octets pathname) stream))

(defun file-to-octets (pathname)
  "PATHNAME is a string or pathname. Return a sequence. If a file is not at the position specified by PATHNAME, return NIL."
  (if (file-or-directory-exists pathname)
      (with-open-file (file-stream
		       pathname
		       :direction :input
		       :element-type '(unsigned-byte 8))
	(let ((seq (make-array (file-length file-stream)
			       :element-type '(unsigned-byte 8))))
	  (read-sequence seq file-stream)
	  seq))))

(defun file-to-string (pathname &key (external-format :utf-8))
  "PATHNAME is a pathname. If PATHNAME points to a file which exists and is readable, return a string corresponding to the content of the file. Signal an error if file contains unreadable data or if the specified file does not exist. Note that 'readable' currently means the file only contains objects corresponding to characters."
  (declare (pathname pathname))
  (assert (uiop:physical-pathname-p pathname))
  (flexi-streams:octets-to-string (file-to-octets pathname)
				  :external-format external-format))

;; convenient SEPARATOR value for grabbing strings delimited by whitespace: '(#\Space #\Newline #\Backspace #\Tab #\Rubout #\Linefeed #\Return #\Page)
(defgeneric file-to-strings (pathname separator)
  (:documentation "Return a list of strings representing sequences of elements in the file specified by PATHNAME. The sequences are separated by sequences matching SEPARATOR. SEPARATOR can be a function or a list of characters. Such characters are discarded and considered to represent boundaries between strings to be returned."))

(defmethod file-to-strings (pathname (separator list))
  (file-to-strings pathname #'(lambda (x) (member x separator :test 'char=))))

(defmethod file-to-strings (pathname (separator function))
  (with-open-file (stream pathname)
    (let ((sanity-count 0)
	  (sanity-cap 5000))
      (let ((strings
	     (loop 
		  ;; SLURP-TO is pretty inefficient for this specific purpose...
		for string? = (slurp-to stream separator)
		  ;; with SLURP-TO, STRING? will be an empty string if (1) at end of file or (2) hitting a series of stream elements that match test
		  ;; - if (1), silently discard but don't terminate loop...
		if (not (string= "" string?)) collect string?
		;; if stuff is still in stream...
		while (ignore-errors (peek-char nil stream t))
		if (> sanity-count sanity-cap) do (return nil)
		  )))
	strings))))

(defun file-to-vector (filespec &key (element-type '(unsigned-byte 8)))
  "Read data at filespec FILESPEC and return the corresponding vector."
  (with-open-file (stream filespec :direction :input :element-type element-type)
    ;; better to make it adjustable and read once (versus using FILE-LENGTH)?
    (let ((vector (make-array 
		   (list (file-length stream))
		   :element-type element-type
		   :adjustable nil)))
      (read-sequence vector stream)
      vector)))

(defun filenames-in-dirpath (dir-path)
  "Given directory specified by the path DIR-PATH, return a list which contains the names of the files in the directory. These names include .xyz suffixes (e.g., 'myfile.tst'). Return nil if the directory does not contain any files."
  (let ((paths (paths-in-dir dir-path))
	(return-list nil))
    (dolist (path paths)
      ;; check if it's a file
      ;; - file-namestring gives file.t
      ;; - pathname-name gives file
      (let ((fname (file-namestring path)))
	(if (not (equal fname ""))
	    ;; could test here with directory-p type of test
	    ;; if we wanted to limit this to files
	    (push fname return-list))))
    return-list))

;; FIXME: shorter, less awkward fn names: FILES-IN-DIR, FILES-OF-TYPE, ...
;; FIXME: wouldn't it be more convenient to have, instead of DIR-PATH, just the directory *component* of a path as the argument
(defun filepaths-in-dirpath (dir-path &key name type)
  "Given directory specified by the path DIR-PATH, return a list which contains the pathnames of the files in the directory. Return nil if the directory does not contain any files."
  (let ((paths (paths-in-dir dir-path))
	(return-list nil))
    (dolist (path paths)
      ;; check if it's a file
      (if (not (equal (file-namestring path) ""))
	  ;; could test here with directory-p type of test
	  ;; if we wanted to limit this to files
	  (if (and (not name) (not type))
	      (push path return-list)
	      (let ((passes-all-tests-p t)) 
		(if name
		    (let ((actual-name (pathname-name path)))
		      (if (not (equalp actual-name name))
			  (setf passes-all-tests-p nil))))		
		(if type
		    (let ((actual-type (pathname-type path)))
		      (if (not (equalp actual-type type))
			  (setf passes-all-tests-p nil))))
		(if passes-all-tests-p
		    (push path return-list))))))
    return-list))

(defun files-of-type (path type)
  "PATH corresponds to a directory. TYPE is a string."
  (declare (string type))
  (filepaths-in-dirpath path :type type))

(defun files-to-file (outfile pathnames)
  "PATHNAMES represent input files. OUTFILE is output file."
  (with-open-file (s outfile
		     :direction :output
		     :if-exists :error
		     :if-does-not-exist :create)
    (files-to-stream s pathnames)))

(defun files-to-stream (stream pathnames)
  "PATHNAMES is a list of strings and/or pathnames."
  (dolist (pathname pathnames)
     (file-to-stream pathname stream)))

(defmacro insert-file-as-string (pathname)
  (file-to-string pathname))

;; facilitate editing javascript as javascript, css as css, etc...
(defmacro insert-system-file-as-string (name system &key type)
  (file-to-string (asdf:system-relative-pathname system name :type type)))

(defun lines-in-file (file &key error-if-range-bad-p end (start 0))
  "Return a sequence where each member represents a line in file FILE. START must be an integer. Return lines from START line to (inclusive) END line (counting begins at 0); if END is NIL continue through the last line in the file. If ERROR-IF-RANGE-BAD-P is true, signal an error if the file does not have one or more lines within the range specified by START and END. Otherwise, in such circumstances, return multiple values: NIL and an integer corresponding to the actual number of lines in the file."
  (declare (integer start))
  (assert (>= start 0))
  (let ((line-number 0))
   (with-open-file (stream file)
     (loop for line = (read-line stream nil nil)
	if (and (null line)
		(or (and end
			 (<= line-number end))
		    (<= line-number start)))
	do (progn
	     (cond (error-if-range-bad-p
		    (error "Unable to satisfy request for ~A lines" (1+ start)))
		   (t
		    (return (values nil line-number)))))
	if (and line
		(>= line-number start)
		(or (not end)
		    (and end (<= line-number end))))
	collect line into lines 
	if (or (null line)
	       (and end (>= line-number end)))
	do (return lines)
	do (incf line-number)))))

(defun load-if-present (pathspec)
  "Execute code at PATHSPEC if PATHSPEC corresponds to a file. Return NIL if unsuccessful. Return T otherwise."
  (ignore-errors (load pathspec) 
		 t))

(defun move-files-to-dir (files dir)
  "FILES is a list of pathnames. DIR is a pathname."
  (declare (list files)
	   (pathname dir))
  (map nil #'(lambda (f)
	       (declare (pathname f))
	       (mv f 
		   (merge-pathnames
			     (make-pathname
			      :name (pathname-name f)
			      :type (pathname-type f))
			     dir)))
       files))

;; This anticipates behavior observed with SBCL with attempts to move files across filesystems.
(defun mv (file destination-file)
  (declare (pathname file destination-file))
  (error "Try UIOP:RENAME-FILE-OVERWRITING-TARGET. Only return to this (broken) if there is a clear shortcoming with UIOP:RENAME-FILE-OVERWRITING-TARGET")
  (handler-case (rename-file file
			     (merge-pathnames
			      (make-pathname
			       :name (pathname-name file)
			       :type (pathname-type file))
			      (make-pathname :directory (pathname-directory destination-file))))
    ;; SBCL chokes on attempting to move file across filesystems with 'Invalid cross-device link' and signals SIMPLE-FILE-ERROR
    (SB-INT:SIMPLE-FILE-ERROR ()
      (uiop:copy-file file destination-file)
      (delete-file file))))

(defun names-in-dir (dir-path)
    "Given directory specified by the path DIR-PATH,return a list which contains the names of the files and directories in the directory."
    (append (filepaths-in-dirpath dir-path) (dirpaths-in-dir dir-path)))

;; FIXME: see also vector-to-file (OCTETS-TO-FILE is more specific...)
(defun octets-to-file (octets pathname &key (if-exists :supersede))
  "Create the file specified by path object PATHNAME (corresponds to first lisp object) such that the file is a text file containing the string SOME-STRING. If the file already exists, it is overwritten ('supersede' on planet lisp). Return value undefined."
  (declare (sequence octets)
	   (pathname pathname))
  (let ((file-stream (open pathname	
			   :direction :output
			   :element-type '(unsigned-byte 8) ; octet
			   :if-exists if-exists
			   :if-does-not-exist :create)))
    (octets-to-stream octets file-stream)
    (close file-stream)))

(defun octets-to-stream (octets stream)
  "Return value undefined."
  (declare (sequence octets)
	   (stream stream))
  (write-sequence octets stream :start 0))

(defun path-modtime (pathname)
  "Return integer representing modification time returned by shell STAT for file or directory represented by string or path PATHNAME. If file or directory does not exist, return nil."
  (if (file-or-directory-exists pathname)
      (parse-integer
       ;; some shells may return explicitly quoted number 
       (string-trim 
	"\"" 
	;; some shells may tag a newline to output
	(string-right-trim 
	 "
"
 	 (uiop:run-program
	  (list "stat"
		"--format=\"%Y\"" 
		;; shell can't handle a lisp path...
		(if (pathnamep pathname)
		    (namestring pathname)
		    pathname)) 
	  :output '(:string :stripped t)))))))

(defun paths-in-dir (dir-path)
  "Return a list of paths in the directory DIR-PATH."
  ;; FIXME: how are symlinks handled?
  (append (uiop:directory-files dir-path) (uiop:subdirectories dir-path)))

(defun plunge-to-file-p (dir-path filename)
  "Given a directory specified by pathname DIR-PATH, follow the left-most branch of the subdirectory tree of DIR-PATH, looking in each subdirectory of the branch for the file named FILENAME. Return T if file present, NIL if file absent."
  (let ((dir
	 ;; first directory path in subdirectory
	 (car (dirpaths-in-dir dir-path))))
    (if dir
	;; then we're not at bottom
	(progn 
	  (if (member filename (filepaths-in-dirpath dir) :test 'equal)
	      ;; file present
	      t
	      ;; file not present, so try again
	      (plunge-to-file-p dir filename)))
	;; we're at bottom
	nil)))

;;    * this function hasn't been even fleshed out yet - non-functional
(defun raw-vector-from-path (pathname)
  "Create and return a vector corresponding to the raw data in the file specified by pathname. Return nil if file specified by pathname is non-existent."
  (if (probe-file pathname)
      ;; file exists
      (progn
	(let (;; stream to use
	      (st (open pathname	
			:direction :input
			:if-does-not-exist :error))
	      ;; vector to return
	      (x (make-array 1 	; how to choose init size??
			     ;:element-type '(unsigned-byte 8)
			     :adjustable t)))
	;; read sequence to vector
	(read-sequence x st)  
	;; return vector
	x))))

(defun read-all-from-file (pathname)
  "Return the lisp objects contained in the file specified by path object PATHNAME. If the file contains multiple lisp objects, they are returned as members of a list. The sexps are ordered in the list with the order reversed relative to their order in the file. Behavior is undefined if a file does not exist at PATHNAME."
  (let ((file-stream
	 (open pathname	
	       :direction :input
	       :if-does-not-exist :error))
	;; list to return
	(return-value nil))
    ;; loop through file
    (do ((x 
	  (read file-stream nil nil)  
	  (read file-stream nil nil)))  
	;; end condition 
 	((not x)
	 (close file-stream)
	 return-value)
      ;; do body
      (push x return-value))))

(defun read-from-file (pathname)
  "Return the lisp object contained in file specified by path object PATHNAME. If PATHNAME contains multiple sexps, only the first is returned. See READ-ALL-FROM-FILE for reading all sexps from a file."
  ;; wouldn't (with-open-file (... serve the same purpose?
  (let* ((file-stream
	  (open pathname	
		:direction :input
		:if-does-not-exist :error))
	 ;; first sexp in file
	 (file-sexp (read file-stream)))
    ;; close stream
    (close file-stream)
    ;; return contents of file
    file-sexp))

(defun tail (pathspec &key (n 10))
  "N is the number of lines to read."
  (declare (integer n))
  (uiop:run-program (list "tail"
			  (format nil "--lines=~A" n)
			  (if (pathnamep pathspec)
			      (namestring pathspec)
			      pathspec))
		    :output '(:string :stripped t)))

;; FIXME: misnomer -- avoid implying 'READ' (CL read...) --> why not just either file-last-line or tail (tail default is last 10 lines...)?
(defun read-last-line (path)
  "Return a string."
  (read-last-line-with-list-accum path))

;; FIXME: should check tail source as it is likely a pretty efficient implementation 
(defun read-last-line-with-list-accum (path)
  (with-open-file (s path)
    (let ((accum nil)
	  (len (file-length s)))
      (do* ((pos (1- len) (1- pos))
	    (char (read-char s (file-position s pos)) (read-char s (file-position s pos)))
	    ;; NIL means that if last character on first read is #\Newline, then we move to grab the preceding line...
	    (newlinep nil (char= char #\Newline)))
	   ((or (<= pos 0) newlinep)
	    (with-output-to-string (return-string-stream)
	      (dolist (c accum)
		(write-char c return-string-stream))))
	(unless newlinep
	  (push char accum))))))

(defun rm-file (path)
  "DELETE-FILE gives an error if file at PATH doesn't exist. RM-FILE removes file if present and doesn't complain if file isn't present."
  (if (file-or-directory-exists path) 
      (delete-file path)))

(defun sexp-from-file (pathname)
  "Deprecated - replace references with references to read-from-file."
 (read-from-file pathname))

(defun sexps-from-file (x)
  (declare (ignore x))
  (error () "Replace with call to READ-ALL-FROM-FILE"))

(defun string-from-file (pathname)
  "Return the first string contained in file specified by path object PATHNAME where the string corresponding to the first lisp object in the file. If the file contains multiple objects, only the string representation (using format ~A) of the first object is returned. The alphanumeric case is preserved. For a function which returns the entire content of a file as a string, see FILE-TO-STRING."
  (let* ((file-stream
	  (open pathname	
		:direction :input
		:if-does-not-exist :error))
	 ;; first sexp in file
	 (file-sexp 
	    ;; preserve case 
	    (let ((current-case (readtable-case *readtable*))
		  (return-value nil))
	      ;; ** bad idea? any other call to lisp during the split second below
	      ;; would throw an error if case doesn't match
	      (setf (readtable-case *readtable*) :PRESERVE)
	      (SETF RETURN-VALUE (READ FILE-STREAM))
	      ;; back to original readtable setting
	      (SETF (READTABLE-CASE *READTABLE*) CURRENT-CASE)
	      return-value)))
    ;; close stream
    (close file-stream)
    ;; return contents of file
    (format nil "~A" file-sexp)))



;; FIXME: note that using PRINC means that SOME-STRING actually doesn't need to be a string -- integers, etc. also work...

;; FIXME: contrast with (alexandria:WRITE-STRING-INTO-FILE) -- what is advantage of PRINC ?

(defun string-to-file (some-string pathname &key (if-exists :supersede))
  "Create the file specified by path object PATHNAME (corresponds to first lisp object) such that the file is a text file containing the string SOME-STRING. If the file already exists, it is overwritten ('supersede' on planet lisp). Return PATHNAME."
  (let ((file-stream (open pathname
			   :direction :output
			   :if-exists if-exists
			   :if-does-not-exist :create)))
    (princ some-string file-stream)
    (close file-stream)
    pathname))

;; FIXME;    ** how is this different than dirnames-in-dir??
(defun subdirectories (directory-path) 
  "Return a list of subdirectory names (not full directory paths)
   given a directory pathname #P/path/to/my/directory."
  (let ((subdir-items
	 (directory 
	  (concatenate
	   'string
	   (namestring directory-path) "*")))
        (subdir-names nil))
    ;; check each subdir-item
    (dolist (subdir-item subdir-items)
      ;; check if it's a directory
      (let ((teststring (namestring (probe-file subdir-item))))
	(if (not (= 
		  (length teststring) 
		  (length (string-right-trim "/" teststring))))
	    ;; it's a directory so use it
	    (let
	      ;; extract directory name from path
	      ((subdir-name
		;;(string (car 
		(last
		 (pathname-directory (pathname subdir-item)))))
	      ;; append current directory name to subdir-names
	      (setf subdir-names (append subdir-names subdir-name))))))
    subdir-names))

;; see also http://www.grumblesmurf.org/corman-patches/sys/streams.lisp (no license info shown)
(defun touch (some-path)
  (unless (file-or-directory-exists some-path) 
    (with-open-file (s some-path
		       :direction :output
		       :if-does-not-exist :create)
      nil))
  ;; thanks to Zach for pointing this out...   
  ;; - use SB-POSIX:TIME for the modification-time - convert universal to unix  
  #+SBCL (SB-POSIX:UTIMES some-path nil (sb-posix:time))
  #+CCL (CCL::TOUCH some-path)
  #+(not (or SBCL CCL)) (error "SBCL and CCL supported")
  )

(defun touch-cl (some-path)
  "If SOME-PATH (a path or a string representing a path) does not exist, create the corresponding file. If the path exists, update the modtime (see shell command touch)."
  (if (probe-file some-path)
      (with-open-file (s some-path
			 :direction :output
			 :if-exists :append)
	(write-string "" s)) 
      (with-open-file (s some-path
			 :direction :output
			 :if-does-not-exist :create)
	nil)))

(defun touch-shell (some-path)
  "If SOME-PATH (a path or a string representing a path) does not exist, create the corresponding file. If the path exists, update the modtime (see shell command touch)."
  (if (probe-file some-path)
      (uiop:run-program (cons "touch"
			      (list
			       (if (pathnamep some-path)
				   (namestring some-path)
				   some-path))))
      (with-open-file (s some-path
			 :direction :output
			 :if-does-not-exist :create) nil)))

(defun vector-to-file (vector pathname &key (element-type '(unsigned-byte 8)) if-exists)
  (with-open-file (stream pathname
                          :direction :output
			  :element-type element-type
			  :if-exists if-exists)
    (write-sequence vector stream)))

(defun wd ()
  "Return a pathname representing the working directory (if the lisp instance was invoked via a shell, where was it invoked?)."
   *default-pathname-defaults*   ;(truename ".") 
   ;; (sb-posix:getcwd) returns a string
  )

(defun wait-for-file-to-exist (pathspec &key error-on-timeout-p (poll 1) (timeout 10))
  "Return when file PATH exists or when timeout is reached.  Timeout (give up) if file is not present by TIMEOUT seconds after calling this function. Poll roughly every POLL seconds. Return NIL if file did not exist at or before the timeout. Return a true value otherwise."
  (declare (number poll timeout))
  (let ((start-time (get-universal-time)))
    (let ((timeout-time (+ timeout start-time)))
      (block check-for-file
	(tagbody tag1
	   (cond ((file-exists-p pathspec)
		  (return-from check-for-file
		    t))
		 ((> (get-universal-time) timeout-time)
		  (if error-on-timeout-p
		      (error "timed out waiting for file")
		      (return-from check-for-file
			nil)))
		 (t
		  (sleep poll)
		  (go tag1))))))))
