(in-package :dfile)
;;;
;;; tools for working with temporary files and directories
;;;
(defun delete-file-in (filespec seconds)
  "Delete file FILE in SECONDS seconds. Return timer."
  (let ((timer (sb-ext::make-timer (lambda () (delete-file filespec)))))
    (sb-ext::schedule-timer timer
			    seconds
			    :absolute-p nil)
    timer))

(defun make-unique-path (dir &key (createp t) directoryp prefix suffix)
  "Return a path object which uniquely represents a full path for a file or directory. DIR is a path object which points to the directory with respect to which the generated path (file or directory) is unique. CREATEP, if true, indicates that the file or directory should be created (the intent is to further reduce the probability (presumably already low) of conflict). If DIRECTORYP is true, return a full path for a directory, ignoring SUFFIX. PREFIX is either NIL or a string.

Note that, although the likely application of MAKE-UNIQUE-PATH is for generating temporary files, the function makes no guarantees regarding the temporary nature of the file or directory corresponding to the path returned.

See also TEMPFILE."
  (let ((path (tmppath* dir :directoryp directoryp :prefix prefix :suffix suffix)))
    (when createp
      (cond (directoryp
	     (ensure-directories-exist path))
	    (t
	     (with-open-file (s path
				:direction :output
				:if-does-not-exist :create)
	       nil))))
    path))

(defun mktemp ()
  "Creates an empty temporary file, safely. Return a string representing the full path corresponding to the temporary file."
  ;; shell mktemp creates a temporary file or directory, safely, and prints its name
  (string-right-trim '(#\Return #\Newline) 
		     (uiop:run-program "mktemp" :output '(:string :stripped t))))


;; FIXME: name of fn doesn't parallel how it acts -- rename as DEFAULT-TEMPDIR
(defun tempdir ()
  "Return a path object corresponding to the default temporary directory on the system."
  uiop:*temporary-directory*)

;; FIXME: resolve existence of this fn, MKTEMP, TMPPATH, etc. --> have single front-end for generating temporary files
(defun tempfile (&key dir directoryp prefix suffix)
  "Return a string corresponding to the complete path pointing to a
temporary file or directory (if DIRECTORYP is true). DIR is a path
object corresponding to a directory (defaults to the equivalent of
/tmp). PREFIX is either NIL or a string. SUFFIX is either NIL or a
string. The corresponding file or directory is not created by invoking
this."
  (tempfile-lisp
   :dir (or dir (tempdir)) 
   :directoryp directoryp
   :prefix prefix
   :suffix suffix))

(defun tempfile-lisp (&key dir directoryp prefix suffix)
  "Return a string."
  (namestring (tmppath* dir :directoryp directoryp :prefix prefix :suffix suffix)))

(defun tempfile-path (&key dir directoryp prefix suffix)
  "Like TEMPFILE, but return a path object."
  (tmppath* (or dir (tempdir)) :directoryp directoryp :prefix prefix :suffix suffix))

;; See also SB-POSIX:MKTEMP.
(defun tmppath* (dir &key directoryp prefix suffix)
  "Return a pathname."
  (assert (or nil (pathnamep dir)))
  (let ((comp "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"))
    (let ((name (with-output-to-string (s)
		  (if prefix (write-string prefix s))
		  (write-string (write-to-string (get-internal-real-time)) s)
		  (dotimes (n 5)
		    (write-char (elt comp (random 62)) s)))))
      (let ((finalname (cond (directoryp (make-pathname :directory (list :relative name)))
			     (suffix (make-pathname :name name :type suffix))
			     (t (make-pathname :name name))))
	    (n-collisions 0)
	    (collisions-max 30))
	(block collisions?
	  (tagbody
	   check-finalname
	     (let ((fullpath (merge-pathnames finalname dir)))
	       ;; PROBE-FILE also checks for existence of directory
	       (if (probe-file fullpath)
		   (go set-finalname)
		   (return-from collisions?
		     fullpath)))
	   set-finalname
	     (incf n-collisions)
	     (if (>= n-collisions collisions-max) (error "Exceeded maximum number of acceptable collisions."))
	     (setf finalname 
		   (concatenate 'string (string (elt comp (random 62)))))
	     (go check-finalname)))))))
